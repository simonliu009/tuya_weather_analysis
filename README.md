# Tuya_weather_analysis

#### readme
This project is developed using Tuya SDK, which enables you to quickly develop branded apps connecting and controlling smart scenarios of many devices.
For more information, please check Tuya Developer Website.
#### 介绍
使用STM32解析涂鸦WIFI模组的天气信息。

#### 主要代码
```C
/**
 * @brief  天气数据用户自处理函数
 * @param[in] {name} 参数名
 * @param[in] {type} 参数类型
 * @ref       0: int 型
 * @ref       1: string 型
 * @param[in] {data} 参数值的地址
 * @param[in] {day} 哪一天的天气  0:表示当天 取值范围: 0~6
 * @ref       0: 今天
 * @ref       1: 明天
 * @return Null
 * @note   MCU需要自行实现该功能
 */
void weather_data_user_handle(char *name, unsigned char type, const unsigned char *data, char day)
{
    //#error "这里仅给出示例，请自行完善天气数据处理代码,完成后请删除该行"
    int value_int;
		int i = 0;
    char value_string[50];//由于有的参数内容较多，这里默认为50。您可以根据定义的参数，可以适当减少该值
    
    my_memset(value_string, '\0', 50);
    
    //首先获取数据类型
    if(type == 0) { //参数是INT型
        value_int = data[0] << 24 | data[1] << 16 | data[2] << 8 | data[3];
    }else if(type == 1) {
        my_strcpy(value_string,(const char*) data);//@
    }
    
    //注意要根据所选参数类型来获得参数值！！！
    if(my_strcmp(name, "temp") == 0) {
        USART2_printf("day:%d temp value is:%d\r\n", day, value_int);          //int 型
        tem = value_int;
    }else if(my_strcmp(name, "humidity") == 0) {
        USART2_printf("day:%d humidity value is:%d\r\n", day, value_int);      //int 型
        hum = value_int;
		}else if(my_strcmp(name, "pm25") == 0) {
        USART2_printf("day:%d pm25 value is:%d\r\n", day, value_int);          //int 型
    }else if(my_strcmp(name, "condition") == 0) {
        //USART2_printf("day:%d condition value is:%d\r\n", day, value_string);  //string 型
        USART2_printf("%x",value_string[0]);
        USART2_printf("%x",value_string[1]);
        USART2_printf("%x",value_string[2]);
        
        while(strcmp(value_string,weather_sheet[i]))
        {
                i++;
        }
        USART2_printf("        %d         ",i);//打印当前天气代码
		}
}
```

#### 问题解答
Q 无法获取天气 或 无法进行解析

A 无法获取天气可能是没有发起请求，在不进行MCU干涉的情况下，天气获取只在初始化时和之后每30分钟下发一次
获取到打开天气获取的回调，却没有天气信息下发，可能是数组过小，建议将数组增大。

#### 使用说明

[视频地址](https://www.bilibili.com/video/BV1XK4y1H7jv)



