#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "usart.h"

#include "mcu_api.h"
#include "protocol.h"
#include "system.h"
#include "wifi.h"


int tem = 0;
int hum = 0;
/************************************************
 串口 涂鸦智能 通信 实验   
 技术支持：717863696 
 作者：刘畅
************************************************/

int time_cnt = 0, cnt = 0, init_flag = 0;
uint32_t flag = 0;
const unsigned char data[50];
void key_scan(void);
void wifi_stat_led(int *cnt);
int main(void)
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); //设置NVIC中断分组2:2位抢占优先级，2位响应优先级
	delay_init();	    	 //延时函数初始化	  
	USART1_Init(9600);	 //串口初始化为115200
	USART2_Init(9600);	 //串口初始化为115200
 	LED_Init();			     //LED端口初始化
	KEY_Init();          //初始化与按键连接的硬件接口
	wifi_protocol_init();//涂鸦智能WIFI模块初始化
	USART2_printf("init ok\r\n");//返回成功
 	while(1)
	{
		if(init_flag == 0)
		{
			time_cnt++;
			if (time_cnt % 6000 == 0)
			{
				time_cnt = 0;
				cnt ++;
			}
			wifi_stat_led(&cnt);   // Wi-Fi状态处理
		}
		wifi_uart_service();
		key_scan();
		
		if(flag > 60000)
		{
			request_weather_serve();//请求天气服务
			USART2_printf("the hum is %d,the tem is %d\r\n",hum,tem);
			flag = 0;
		}
		flag ++;
		delay_ms(1);
	}	 
}
void key_scan(void)
{
  static char ap_ez_change = 0;
  
  if(KEY_Config_Read() == 0){
    delay_ms(3000);
    if (KEY_Config_Read() == 0) {
      init_flag = 0;
      switch (ap_ez_change) {
        case 0 :
          mcu_set_wifi_mode(SMART_CONFIG);
          break;
        case 1 :
          mcu_set_wifi_mode(AP_CONFIG);
          break;
        default:
          break;
      }
      ap_ez_change = !ap_ez_change;
    }

  }
}

void wifi_stat_led(int *cnt)
{
  switch (mcu_get_wifi_work_state())
  {
    case SMART_CONFIG_STATE:  //0x00
      init_flag = 0;
      if (*cnt == 2) {
        *cnt = 0;
      }
      if (*cnt % 2 == 0)  //LED快闪
      {
        LED_WIFI_on();
      }
      else
      {
        LED_WIFI_off();
      }
      break;
    case AP_STATE:  //0x01
      init_flag = 0;
      if (*cnt >= 30) {
        *cnt = 0;
      }
      if (*cnt  == 0)      // LED 慢闪
      {
        LED_WIFI_on();
      }
      else if (*cnt == 15)
      {
        LED_WIFI_off();
      }
      break;

    case WIFI_NOT_CONNECTED:  // 0x02
      LED_WIFI_off();//LED熄灭
      break;
    case WIFI_CONNECTED:  // 0x03
      break;
    case WIFI_CONN_CLOUD:  // 0x04
      if ( 0 == init_flag )
      {
        LED_WIFI_on();// LED 常亮
        init_flag = 1;                  // Wi-Fi 连接上后该灯可控
        *cnt = 0;
      }

      break;

    default:
      LED_WIFI_off();//LED熄灭
      break;
  }
}
